loaders package
===============

Subpackages
-----------

.. toctree::

   loaders.colours

Module contents
---------------

.. automodule:: loaders
   :members:
   :undoc-members:
   :show-inheritance:
