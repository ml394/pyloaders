from loaders import *
from datetime import datetime

class Examples(object):

    def __init__(self, mode='fixed', iterations=5):
        self.mode = str(mode)
        self.i = iterations
        self.indeterminate = [
            Loader(),
            SpinningLoader(text='Waiting...', speed=.25, colour='red'),
            TextLoader(speed=.25, animation='bounce', colour='blue', style='bold'),
            TextLoader(text='Hello World', speed=.25, size='large', character='*'),
            BarLoader(speed=.1, animation='bounce', colour='yellow'),
            BarLoader(speed=.1, colour='green', character="#")
        ]
        self.determinate = [
            ProgressLoader(total=5, colour='blue', style='bold'),
            ProgressLoader(total=5, colour='orange', character='>')
        ]

    def run_indeterminate(self):
        for loader in self.indeterminate:
            if self.mode.lower() == 'thread':
                print('Testing out %s. It should stop after %ds' % (loader, self.i))
                loader.start()
                for i in range(self.i):
                    time.sleep(1)
                loader.stop()
                print('Now onto some other tasks...')
            else:
                print('%s started at %s' % (loader, str(datetime.now())))
                loader.duration = self.i
                loader.run()

    def run_determinate(self):
        for loader in self.determinate:
            print('%s started at %s' % (loader, str(datetime.now())))
            for i in range(loader.total + 1):
                loader.progress(i)
                time.sleep(.5)

    def run(self):
        self.run_indeterminate()
        self.run_determinate()

def play():
    if len(sys.argv) > 1:
        if str(sys.argv[1].lower()) == 'thread':
            Examples(mode='thread').run()
        else:
            print('Invalid argument: %s' % str(sys.argv[1]))
    else:
        Examples().run()

play()
