from loaders import *

DURATION = 1
SPEED = .1

def run(loader):
    try:
        if loader.duration:
            loader.run()
        else:
            loader.start()
            for i in range(1):
                time.sleep(DURATION)
            loader.stop()
        return True
    except:
        return False


def test_loader_colours():
    assert(SpinningLoader(colour="blue").colour == "\033[94m")
    assert(TextLoader(colour="green").colour == "\033[92m")
    assert(BarLoader(colour="yellow").colour == "\033[93m")
    assert(TextLoader(colour="orange").colour == "\033[93m")
    assert(BarLoader(colour="red").colour == "\033[91m")
    assert(SpinningLoader(colour="bad input").colour == "")
    assert(Loader().colour == "\033[91m")

def test_loader_styles():
    assert(SpinningLoader(style="header").style == "\033[95m")
    assert(TextLoader(style="bold").style == "\033[1m")
    assert(BarLoader(style="underline").style == "\033[4m")
    assert(SpinningLoader(style="bad input").style == "")
    assert(Loader().style == "\033[1m")

def test_loader_str():
    assert(str(Loader()) == "Loader")
    assert(str(SpinningLoader()) == "SpinningLoader")
    assert(str(TextLoader()) == "TextLoader")
    assert(str(BarLoader()) == "BarLoader")
    assert(str(ProgressLoader()) == "ProgressLoader")

def test_text_loader_animation():
    print("Testing text bounce")
    speed = SPEED
    loader = TextLoader(text='test', size="small", speed=speed, animation='bounce')
    loader_loop = TextLoader(text='test', size="small", speed=speed)
    loader.start()
    loader_loop.start()
    assert(loader.direction == 'ltr')
    assert(loader_loop.direction == 'ltr')
    time.sleep(speed * 4)
    assert(loader.direction == 'rtl')
    assert(loader_loop.direction == 'ltr')
    time.sleep(speed * 4)
    assert(loader.direction == 'ltr')
    assert(loader_loop.direction == 'ltr')
    loader.stop()
    loader_loop.stop()

def test_bar_loader_animation():
    print("Testing bar bounce")
    speed = SPEED
    loader = BarLoader(speed=speed, size="small", animation="bounce")
    loader_loop = BarLoader(speed=speed, size="small")
    loader.start()
    loader_loop.start()
    assert(loader.direction == 'ltr')
    assert(loader_loop.direction == 'ltr')
    time.sleep(speed * 10)
    assert(loader.direction == 'rtl')
    assert(loader_loop.direction == 'ltr')
    time.sleep(speed * 10)
    assert(loader.direction == 'ltr')
    assert(loader_loop.direction == 'ltr')
    loader.stop()
    loader_loop.stop()

def test_text_loader_length():
    assert(TextLoader(text='test', size='small').get_length() == 8)
    assert(TextLoader(text='test', size='medium').get_length() == 12)
    assert(TextLoader(text='test', size='large').get_length() == 20)

def test_bar_loader_length():
    assert(BarLoader(size='small').get_length() == 20)
    assert(BarLoader(size='medium').get_length() == 50)
    assert(BarLoader(size='large').get_length() == 100)

def test_loader_thread_start():
    loader = Loader()
    assert(loader.thread is None)
    start = loader.start()
    assert(start is True)
    assert(loader.thread is not None)
    second_start = loader.start()
    assert(second_start is False)
    loader.stop()
    assert(loader.thread is None)

def test_loader_thread_stop():
    loader = Loader()
    loader.start()
    assert(loader.thread is not None)
    stop = loader.stop()
    assert(stop is True)
    assert(loader.thread is None)
    second_stop = loader.stop()
    assert(second_stop is False)

def test_loader_thread_terminate():
    all = a, b, c, d = Loader(), SpinningLoader(), TextLoader(), BarLoader()
    for loader in all:
        assert(loader.thread is None)
        try:
            loader.terminate()
            can_terminate = True
        except ValueError as e:
            assert(str(e) == "No thread to terminate")
            can_terminate = False
        assert(can_terminate is False)

def test_loader_run_thread():
    assert(run(Loader(speed=SPEED)) == True)

def test_loader_run_fixed():
    assert(run(Loader(duration=DURATION)) == True)

def test_spinning_loader_run_thread():
    assert(run(SpinningLoader(speed=SPEED)) == True)

def test_spinning_loader_run_fixed():
    assert(run(SpinningLoader(duration=DURATION)) == True)

def test_text_loader_run_thread():
    assert(run(TextLoader(speed=SPEED)) == True)

def test_text_loader_run_fixed():
    assert(run(TextLoader(duration=DURATION)) == True)

def test_bar_loader_run_thread():
    assert(run(BarLoader(speed=SPEED)) == True)

def test_bar_loader_run_fixed():
    assert(run(BarLoader(duration=DURATION)) == True)

def test_progress_loader_run():
    assert(ProgressLoader().run() is None)

def test_progress_loader_run_thread():
    loader = ProgressLoader(total=10)
    try:
        for i in range(loader.total + 1):
            loader.progress(i)
            time.sleep(SPEED)
        complete = True
    except:
        complete = False
    assert(complete == True)
