.. pyLoaders documentation master file, created by
   sphinx-quickstart on Fri Aug 16 01:58:15 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyLoaders's documentation!
=====================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   loaders


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
