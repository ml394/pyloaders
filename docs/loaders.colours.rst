loaders.colours package
=======================

Module contents
---------------

.. automodule:: loaders.colours
   :members:
   :undoc-members:
   :show-inheritance:
